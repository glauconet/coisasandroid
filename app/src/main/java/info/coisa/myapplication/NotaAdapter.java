package info.coisa.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import info.coisa.myapplication.Dominio.Nota;

import java.util.List;

/**
 * Created by glauco on 15/02/16.
 */
public class NotaAdapter extends RecyclerView.Adapter <NotaAdapter.NotaViewHolder>{


    private List<Nota> notas;

    private final Context context;
    private final NotaOnClickListener onClickListener;

    public interface NotaOnClickListener{

        public void onClickNota(View view, int idx);
    }


    public NotaAdapter(Context context, List<Nota> notas, NotaOnClickListener onClickListener) {

        Log.i("log","NotaAdapter constructor");
        this.context = context;
        this.onClickListener = onClickListener;
        this.notas = notas;
    }

    @Override
    public NotaViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //Log.i("log","NotaAdapter constructor");
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_nota, viewGroup, false);

            NotaViewHolder holder = new NotaViewHolder(view);
            return   holder;
    }

    @Override
    public void onBindViewHolder(final NotaViewHolder holder, final int position) {

        Nota  n = notas.get(position);
        holder.nome.setText(n.nome);

        if(onClickListener != null){


            holder.itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    onClickListener.onClickNota(holder.view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        return this.notas != null ? this.notas.size() : 0;
    }


    public static class NotaViewHolder extends RecyclerView.ViewHolder{


        public TextView nome;

        private View view;
        private ImageView img;

        public NotaViewHolder(View view) {
            super(view);

            this.view = view;
            nome = (TextView) view.findViewById(R.id.nome_nota);
            img = (ImageView) view.findViewById(R.id.img);

        }
    }
}
