package info.coisa.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import info.coisa.myapplication.Service.NotaService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by glauco on 18/02/16.
 */
public class NovaNotaActivity extends ActivityAbstrata{

    EditText textView_nome_anotacao;
    EditText textView_anotacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_nota);
        textView_anotacao = (EditText) findViewById(R.id.anotacao);
        textView_nome_anotacao = (EditText) findViewById(R.id.nome_anotacao);

        Intent intent = getIntent();
        if(intent.getStringExtra("nome") != null) {
            Log.i("log", "Entrou no get intent");
            String nome = intent.getStringExtra("nome");
            String output = nome.substring(0,nome.indexOf('.'));
            textView_nome_anotacao.setText(output);
            textView_anotacao.setText(getTextoAnotacao(nome));
        }
        setUpToolbar();
    }

    private String getTextoAnotacao(String nome)  {

        Log.i("log", "getTextoAnotacao");

        File documentos = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);

        File file = new File(documentos,nome);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(text).toString();
    }


    public void onClickEnviarNota(View view) {

        String nome_anotacao = textView_nome_anotacao.getText().toString();
        String anotacao = textView_anotacao.getText().toString();

        Log.i("log", getLocalClassName() + nome_anotacao.toString() );
        Log.i("log", getLocalClassName() + anotacao.toString() );

        NotaService.salvarArquivoSD(getApplicationContext(), nome_anotacao +".txt",anotacao);
        finish();
    }

}
