package info.coisa.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.*;
import info.coisa.myapplication.Service.UtilHttp;

import java.io.File;


public class UploadActivity extends ActivityAbstrata {


    private String[] tipos = new String[]{"Nota", "Foto", "Som", "Vídeo"};

    private String[] listaArquivos = new String[]{"não caregado"};

    Spinner comboArquivo = null;

    ArrayAdapter<String> adptadorArquivo = null;

    File diretorio = null;

    String arquivo = null;

    TextView textViewArquivo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        final Spinner comboTipo = (Spinner) findViewById(R.id.spinner_tipo);
        textViewArquivo = (TextView) findViewById(R.id.arquivo);
        ArrayAdapter<String> adaptadorTipo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tipos);
        setUpToolbar();
        adaptadorTipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        comboTipo.setAdapter(adaptadorTipo);
        comboTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                carregar(posicao);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        comboArquivo = (Spinner) findViewById(R.id.spinner_arquivo);
        comboArquivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                arquivo = comboArquivo.getSelectedItem().toString();
                textViewArquivo.setText(arquivo);
                Log.i("log", "Pasta e Arquivo" + String.valueOf(diretorio) + "/"+ arquivo);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void startUploading(View view) {

        File file = new File(diretorio,arquivo);
        Log.i("log", "    File para upload     " + String.valueOf(diretorio) + "/"+ arquivo);


    }

    public void carregar(int id){
        switch (id){
            case 0:
                diretorio = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                break;
            case 1:
                diretorio = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                break;
            case 2:
                diretorio = getExternalFilesDir(Environment.DIRECTORY_MUSIC);
                break;
            case 3:
                diretorio = getExternalFilesDir(Environment.DIRECTORY_MOVIES);
                break;
            default:
                ;
                break;
        }

        listaArquivos = diretorio.list();
        //for (String s: listaArquivos){Log.i("log", s); }
        adptadorArquivo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listaArquivos);
        adptadorArquivo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        comboArquivo.setAdapter(adptadorArquivo);
    }


    private void enviarFoto() {

        Log.i("log", "    incicio de enviarFoto File para upload ");

        new UploadArquivoTask().execute(arquivo, String.valueOf(diretorio) + "/"+ arquivo);

        Log.i("log", "   fim  de enviarFoto ");
    }
    class UploadArquivoTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            boolean sucesso = false;

            try {
                Log.i("log", "UploadArquivoTask TRY" );
                UtilHttp.enviarFoto(UploadActivity.this, strings[0], /* texto */ strings[1]  /* caminho do arquivo*/);
                sucesso = true;
                Log.i("log", "UploadArquivoTask TRY" + sucesso );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sucesso;
        }
        @Override
        protected void onPostExecute(Boolean sucesso) {

            Log.i("log", "onPostExecute" );
            super.onPostExecute(sucesso);
            String retorno = null;

            if(sucesso){
                retorno = "sucesso";
            }else{
                retorno = "falha";
            }

            Log.i("log",retorno);
            Log.i("log", "onPostExecute ---------. FIMMMMM" );
        }
    }

    /*
    *
    * File documentos = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);

        File file = new File(documentos,nome);
    *
    *
    *  File file = getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        mFileName = file.getAbsolutePath() + "/" + s + ".mp3";
        Log.i("log",mFileName);
    * */


    //@Override
   /* public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_upload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
