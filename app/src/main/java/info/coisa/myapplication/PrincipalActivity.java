package info.coisa.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

public class PrincipalActivity extends ActivityAbstrata
        implements NavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i("log","ON CREATE" + getLocalClassName());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        Log.i("log","onBackPressed - botao voltar");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent ;

        switch (id) {
            case R.id.nav_nota:
                intent =   new Intent(this, NotaActivity.class);
                startActivity(intent);// or startActivityForResult
                break;
            case R.id.nav_fotos:
                intent =   new Intent(this, FotoActivity.class);
                startActivity(intent);// or startActivityForResult
                ;
                break;
            case R.id.nav_som:
                intent =   new Intent(this, SomActivity.class);
                startActivity(intent);// or startActivityForResult
                break;

            case R.id.nav_videos:
                intent =   new Intent(this, VideoActivity.class);
                startActivity(intent);// or startActivityForResult

                break;
            case R.id.nav_rest:
                intent =   new Intent(this, RestActivity.class);
                startActivity(intent);// or startActivityForResult

                break;
            default:
                ;
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void onClickNovaNota(View view) {
        Intent i = null;
        if(view.getId() == R.id.botao_nova_nota) {
            i =   new Intent(this, NovaNotaActivity.class);
        }
        if(view.getId() == R.id.botao_nova_foto) {
            i =   new Intent(this, NovaFotoActivity.class);
        }
        if(view.getId() == R.id.botao_novo_som) {

            i =   new Intent(this, NovoSomActivity.class);
        }
        if(view.getId() == R.id.botao_novo_video) {

            Log.i("log","ON botao_novo_video----" );
            i =   new Intent(this, RestActivity.class);

        }
        if(view.getId() == R.id.botao_upload) {

            Log.i("log","ON botao_upload----" );

            i =   new Intent(this, UploadActivity.class);

        }
        if(view.getId() == R.id.botao_download) {

            Log.i("log","ON botao_download----" );
            i =   new Intent(this, RestActivity.class);

        }

        startActivity(i);
    }
}
