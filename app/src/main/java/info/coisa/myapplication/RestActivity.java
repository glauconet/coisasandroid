package info.coisa.myapplication;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestActivity extends ActivityAbstrata implements  Runnable{


    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpToolbar();
        Log.d("log", "ON CREATE");
        Button button = (Button) findViewById(R.id.botao);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rest, menu);
        return true;
    }

    public void onBtnClicked(View view) {
        Toast.makeText(getApplicationContext(), "Botão clicado ", Toast.LENGTH_SHORT).show();
        Log.d("log", "onBtnClicked");
        //TextView urlTextView = (TextView) findViewById(R.id.url);
        //TextView dadosTextView = (TextView) findViewById(R.id.dados);

        // String url = urlTextView.getText().toString();
        //dadosTextView.setText((CharSequence) url.toString());

        new Thread(this).start();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){

            finish();
            return  true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Menu com Toast", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == R.id.action_settings1) {
            Toast.makeText(getApplicationContext(), "Menu com Toast1", Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.action_settings2) {
            Toast.makeText(getApplicationContext(), "Menu com Toast2", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void run() {
        Log.d("log", "run");
        try {

            String USER_AGENT = "Mozilla/5.0";


            String url = "http://coisa.info/api/verbs/?categoria=most";
            //TextView urlTextView = (TextView) findViewById(R.id.url);
            //String url = urlTextView.getText().toString();


            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            TextView dadosTextView = (TextView) findViewById(R.id.dados);

            dadosTextView.setText((CharSequence) response.toString());
            Log.d("log", response.toString());


        } catch (Throwable e) {


        } finally {


        }
        Log.d("log", " fim run");
        //this.outroTeste();
    }

    public void outroTeste(){

        Client client = ClientBuilder.newClient();


        WebTarget target = client.target("http://coisa.info/api/verbs/?categoria=most");
       Response response = target.request().get();
        String cotacao = response.readEntity(String.class);
       System.out. println (cotacao);

        Log.d("log", "run");
    }

}
