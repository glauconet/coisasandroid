package info.coisa.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import info.coisa.myapplication.Dominio.Nota;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class NotaActivity extends ActivityAbstrata {

    private RecyclerView recyclerview;
    private ProgressBar progressBar;
    private List<Nota> notas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerview.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getBaseContext()).build());//from https://android-arsenal.com/details/1/1418
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setHasFixedSize(true);
        setUpToolbar();
        carregarNotas();
    }

    public void carregarNotas(){
        new Thread(){
            public void run(){
                try{
                    notas = getNotas();
                    atualizarRecyclerView();
                }catch (Exception e ){
                    Log.i("log ", e.getMessage(), e);
                }
            }
        }.start();
    }

    public void atualizarRecyclerView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                recyclerview.setAdapter(new NotaAdapter (getBaseContext(), notas, onClickNotas()));

            }
        });
    }


    private NotaAdapter.NotaOnClickListener onClickNotas() {
        return new NotaAdapter.NotaOnClickListener() {
            @Override
            public void onClickNota(View view, int idx){
                List<Nota> lista = getNotas();
                Nota n = lista.get(idx);
                Log.i("log" , n.nome);
                Intent i =   new Intent(getBaseContext(), NovaNotaActivity.class);
                i.putExtra("nome",n.nome);
                startActivity(i);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nota, menu);//
        return true;
    }

    private List<Nota> getNotas(){
        List<Nota> notas = new ArrayList<Nota>();
        Log.i("log", getFilesDir().toString());
        File fileCerto = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        String sdpath, sd1path, usbdiskpath, sd0path;

        if(new File("/storage/external_SD/").exists()){
            sd0path="/storage/external_SD/";
            Log.i("log","Sd Card0 Path: " +sd0path);
            File f = new File(sd0path);
            File file[] = f.listFiles();
            Log.i("log", "Size: "+ file.length);
            for (int i=0; i < file.length; i++) {
                Log.i("log", "FileName: " + file[i].getName());
            }
        }
        String[] lista = fileCerto.list();
        for (String s: lista){
            Log.i("log", s);
            notas.add(new Nota(s, R.drawable.nota));
        }
        return notas;
    }
}
