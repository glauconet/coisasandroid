package info.coisa.myapplication.Adapters;

import android.content.Context;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import info.coisa.myapplication.Dominio.Audio;
import info.coisa.myapplication.Dominio.Nota;
import info.coisa.myapplication.R;

import java.util.List;

/**
 * Created by glauco on 18/02/16.
 */
public class SomAdapter extends RecyclerView.Adapter <SomAdapter.AudioViewHolder>{


    private List<Audio> audios;

    private final Context context;
    private final AudioOnClickListener onClickListener;

    public interface AudioOnClickListener{

        public void onClickAudio(View view, int idx);
    }


    public SomAdapter(Context context, List<Audio> audios, AudioOnClickListener onClickListener) {

        Log.i("log","NotaAdapter constructor");
        this.context = context;
        this.onClickListener = onClickListener;
        this.audios = audios;
    }




    @Override
    public AudioViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_nota, viewGroup, false);

        AudioViewHolder holder = new AudioViewHolder(view);
        return   holder;



    }

    @Override
    public void onBindViewHolder(final AudioViewHolder holder, final int position) {
        Audio  n = audios.get(position);
        holder.nome.setText(n.nome);

        if(onClickListener != null){

            holder.itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    onClickListener.onClickAudio(holder.view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        return this.audios != null ? this.audios.size() : 0;
    }

    public static class AudioViewHolder extends RecyclerView.ViewHolder{


        public TextView nome;

        private View view;
        private ImageView img;

        public AudioViewHolder(View view) {
            super(view);

            this.view = view;
            nome = (TextView) view.findViewById(R.id.nome_nota);
            img = (ImageView) view.findViewById(R.id.img);

        }
    }

}
