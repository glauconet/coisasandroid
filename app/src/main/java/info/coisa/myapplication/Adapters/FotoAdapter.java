package info.coisa.myapplication.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import info.coisa.myapplication.Dominio.Foto;
import info.coisa.myapplication.FotoActivity;
import info.coisa.myapplication.R;
import livroandroid.lib.utils.ImageResizeUtils;

import java.io.File;
import java.util.List;

/**
 * Created by glauco on 18/02/16.
 */
public class FotoAdapter extends RecyclerView.Adapter <FotoAdapter.FotoViewHolder>{


    private final Context context;
    private List<Foto> fotos;
    private final FotoOnClickListener onClickListener;
    public interface FotoOnClickListener {
        public void onClickFoto(View view, int idx);
    }

    public FotoAdapter(Context context, List<Foto> fotos, FotoOnClickListener OnClickListener) {
        this.context = context;
        this.fotos = fotos;
        this.onClickListener = OnClickListener;
    }

    @Override
    public FotoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_foto, viewGroup, false);
        FotoViewHolder holder = new FotoViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final FotoViewHolder holder, final int position) {

        Log.d("log", "-----onBindViewHolder-------");

        Foto f = fotos.get(position);

        holder.nome.setText(f.nome);

        Bitmap myBitmap = BitmapFactory.decodeFile(f.file.getAbsolutePath());
        Bitmap myBitmapResized = Bitmap.createScaledBitmap(myBitmap,(int)(myBitmap.getWidth()*0.8), (int)(myBitmap.getHeight()*0.8), true);

        holder.img.setImageBitmap(myBitmapResized);



        if(onClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v){

                    onClickListener.onClickFoto(holder.view, position);
                }

            });


        }

    }

    @Override
    public int getItemCount() {
        return this.fotos != null ? this.fotos.size() : 0;
    }



    public static class FotoViewHolder extends RecyclerView.ViewHolder{

        public TextView nome;
        private ImageView img;
        private View view;


        public FotoViewHolder(View view) {
            super(view);
            this.view = view;
            nome = (TextView) view.findViewById(R.id.nome_foto);
            img = (ImageView) view.findViewById(R.id.img);


        }
    }

}
