package info.coisa.myapplication;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;

import java.util.List;

/**
 * Created by glauco on 18/02/16.
 */
public class VideoActivity extends ActivityAbstrata {

    private RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i("log","ON CREATE" + getLocalClassName());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video);

        setUpToolbar();


        //recyclerview.setAdapter(new NotaAdapter (this, notas, onClickNotas()));

    }
    // fazer o adpter


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video, menu);
        return true;
    }


}
