package info.coisa.myapplication;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import info.coisa.myapplication.Adapters.FotoAdapter;
import info.coisa.myapplication.Dominio.Foto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FotoActivity extends ActivityAbstrata {

    private RecyclerView recyclerview;
    private ProgressBar progressBar;
    List<Foto> fotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerview.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getBaseContext()).build());
        //from https://android-arsenal.com/details/1/1418
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setHasFixedSize(true);


        setUpToolbar();
        carregarFotos();
    }

    public void carregarFotos(){
        new Thread(){
            public void run(){
                try{
                    fotos = getFilesFotos();
                    atualizarRecyclerView();
                }catch (Exception e ){
                    Log.i("log ", e.getMessage(), e);
                }
            }
        }.start();
    }

    private void atualizarRecyclerView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressBar.setVisibility(View.INVISIBLE);
                recyclerview.setAdapter(new FotoAdapter(getBaseContext(), fotos, onClickFoto()));
            }
        });
    }

    private List<Foto> getFilesFotos() {
        List<Foto> fotos = new ArrayList<Foto>();
        File fileCerto = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File files[] = fileCerto.listFiles();
        for (File file: files){
            Log.i("log", file.getAbsolutePath());
            fotos.add(new Foto(file.getName(),file));
        }
        return  fotos;
    }

    private FotoAdapter.FotoOnClickListener onClickFoto() {
        return new FotoAdapter.FotoOnClickListener(){
            @Override
            public void onClickFoto(View view, int idx){
                List<Foto> fotos = getFilesFotos();
                Foto f = fotos.get(idx);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_foto, menu);
        return true;
    }
}
