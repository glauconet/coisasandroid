package info.coisa.myapplication;

import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import info.coisa.myapplication.Service.PlayerMp3;

import java.io.File;
import java.io.IOException;

/**
 * Created by glauco on 23/02/16.
 */
public class NovoSomActivity extends ActivityAbstrata{

    private PlayerMp3 player = new PlayerMp3();
    private EditText text;

    private static String mFileName = null;
    private MediaRecorder mRecorder;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_audio);
        text  = (EditText)findViewById(R.id.arquivo);

        Intent intent = getIntent();
        if(intent.getStringExtra("nome") != null) {
            Log.i("log", "Entrou no get intent");
            String nome = intent.getStringExtra("nome");
            String output = nome.substring(0,nome.indexOf('.'));
            text.setText(output);
        }
        setUpToolbar();
    }

    //player methods
    public void onClickStart(View view) {
        String s = text.getText().toString();
        File file = getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        mFileName = file.getAbsolutePath() + "/" + s + ".mp3";
        Log.i("log",mFileName);
        player.start(mFileName);
    }

    public void onClickPause(View view) {
        player.pause();
    }

    public void onClickStop(View view) {
        player.stop();
    }

    //record methods

    public void startRecording(View view) {
        Log.i("log","startRecording");
        mRecorder = new MediaRecorder();

        String s = text.getText().toString();

        File file = getExternalFilesDir(Environment.DIRECTORY_MUSIC);

        mFileName = file.getAbsolutePath() + "/" + s + ".mp3";

        Log.i("log",mFileName);


        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.i("log", "prepare() failed");
        }

        mRecorder.start();
    }


    public void stopRecording(View view) {
        if(mRecorder != null){
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;

        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }


    }
}
