package info.coisa.myapplication.Service;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import livroandroid.lib.utils.FileUtils;
import livroandroid.lib.utils.IOUtils;
import livroandroid.lib.utils.SDCardUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by glauco on 19/02/16.
 */
public class NotaService {



    public  static void salvarArquivoSD(Context context, String url, String json){

        String fileName = url.substring(url.lastIndexOf("/") +1);

        File f = getPrivateFile(context, fileName, Environment.DIRECTORY_DOCUMENTS);

        writeString(f, json);

        //Log.i("log", "Arquivo gravado em Downloads Privado" + f);

        //f = getPublicFile(fileName, Environment.DIRECTORY_DOWNLOADS);

       // writeString(f,json);

        Log.i("log", "Arquivo gravado em Downloads Público" + f);




    }


    public static void salvarArquivo(Context context, String url, String json){
        String fileName = url.substring(url.lastIndexOf("/")+1);
        //File file = FileUtils.getFile(context, fileName);//IOUtils.writeString(file, json);
        File file = getFile(context,fileName);
        writeString(file, json);

        Log.i("log","Arquivo salvo: " + file );


    }

    private static File getFile(Context context, String name) {
        File file = context.getFileStreamPath(name);
        return file;
    }


    private static void writeString(File file, String string) {
        writeBytes(file, string.getBytes());
    }

    public static void writeBytes(File file, byte[] bytes) {
        try {
            FileOutputStream e = new FileOutputStream(file);
            e.write(bytes);
            e.flush();
            e.close();
        } catch (IOException var3) {
            Log.e("IOUtils", var3.getMessage(), var3);
        }

    }

    public static File getPublicFile(String fileName, String type) {
        File sdCardDir = Environment.getExternalStoragePublicDirectory(type);
        return createFile(sdCardDir, fileName);
    }

    private static File createFile(File sdCardDir, String fileName) {
        if(!sdCardDir.exists()) {
            sdCardDir.mkdir();
        }

        File file = new File(sdCardDir, fileName);
        return file;
    }

    public static File getPrivateFile(Context context, String fileName, String type) {
        File sdCardDir = context.getExternalFilesDir(type);
        return createFile(sdCardDir, fileName);
    }
}
