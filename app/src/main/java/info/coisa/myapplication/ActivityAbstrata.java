package info.coisa.myapplication;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

/**
 * Created by glauco on 18/02/16.
 */
public abstract class ActivityAbstrata extends AppCompatActivity {


    protected void setUpToolbar(){

        Toolbar toolbar = (Toolbar)  findViewById(R.id.toolbar) ;
        if(toolbar != null){
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
            return  true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i("log", getClassName() + ".onStart() chamado");


    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i("log","ON RESUME - bom lugar para fazer threads que consultam webservices ou bancos de dados");
        Log.i("log", getClassName() + ".onResume() chamado");


    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.i("log","ON PAUSE");
        Log.i("log", getClassName() + ".onPause() chamado");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.i("log","ON RESTART");
        Log.i("log", getClassName() + ".onRestart() chamado");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.i("log","ON STOP - chamado depois onPause");
        Log.i("log", getClassName() + ".onStop() chamado");
    }

    private String getClassName(){
        String s = getClass().getName();
        return s.substring(s.lastIndexOf("."));
    }
    protected void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        Log.i("log","if spin the smartphone");
        outState.putString("teste","teste" );

    }


}
