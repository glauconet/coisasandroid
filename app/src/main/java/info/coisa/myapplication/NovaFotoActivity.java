package info.coisa.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import livroandroid.lib.utils.ImageResizeUtils;
import livroandroid.lib.utils.SDCardUtils;

import java.io.File;

/**
 * Created by glauco on 18/02/16.
 */
public class NovaFotoActivity extends ActivityAbstrata{

    private File file;
    private ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i("log","ON CREATE" + getLocalClassName());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nova_foto);

        setUpToolbar();
        imgView = (ImageView) findViewById(R.id.imagem);


        if(savedInstanceState != null){

            file = (File) savedInstanceState.getSerializable("file");
            showImage(file);
        }

    }




    public void onClickNovaFoto(View v) {

        TextView nome_nova_foto = (TextView) findViewById(R.id.nome_nova_foto);

        String s = nome_nova_foto.getText().toString();

        file = SDCardUtils.getPrivateFile(getBaseContext(), s + ".jpg", Environment.DIRECTORY_PICTURES);

        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

        startActivityForResult(i, 0);
    }






    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        // Salvar o estado caso gire a tela
        outState.putSerializable("file", file);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && file != null) {
            showImage(file);
        }
    }

    private void showImage(File file) {
        if (file != null && file.exists()) {

            Log.d("log", file.getAbsolutePath());

            int w = imgView.getWidth();
            int h = imgView.getHeight();
            Bitmap bitmap = ImageResizeUtils.getResizedImage(Uri.fromFile(file), w, h, false);
            Toast.makeText(this, "w/h:" + imgView.getWidth() + "/" + imgView.getHeight() + " > " + "w/h:" + bitmap.getWidth() + "/" + bitmap.getHeight(), Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "file:" + file, Toast.LENGTH_SHORT).show();

            imgView.setImageBitmap(bitmap);
        }
    }

}
