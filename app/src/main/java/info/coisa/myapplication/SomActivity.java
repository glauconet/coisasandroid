package info.coisa.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import info.coisa.myapplication.Adapters.SomAdapter;
import info.coisa.myapplication.Dominio.Audio;
import info.coisa.myapplication.Dominio.Nota;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SomActivity extends ActivityAbstrata {

    private RecyclerView recyclerview;
    private ProgressBar progressBar;
    private List<Audio> audios;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_som);
        progressBar = (ProgressBar) findViewById(R.id.progress_som);
        progressBar.setVisibility(View.VISIBLE);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setHasFixedSize(true);
        setUpToolbar();
        carregarAudios();
    }

    private void carregarAudios() {
        new Thread(){
            public void run(){
                try{
                    audios = getAudios();
                    atualizarRecyclerView();
                }catch (Exception e ){
                    Log.i("log ", e.getMessage(), e);
                }
            }
        }.start();
    }



    private void atualizarRecyclerView() {
        Log.i("log" ,"  A recyclerView sera atualizada aqui" );
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                recyclerview.setAdapter(new SomAdapter(getBaseContext(), audios, onClickAudio()));
            }
        });

    }

    private SomAdapter.AudioOnClickListener onClickAudio() {
        return new SomAdapter.AudioOnClickListener() {
            @Override
            public void onClickAudio(View view, int idx){
                List<Audio> lista = getAudios();
                Audio n = lista.get(idx);
                Log.i("log" , n.nome);
                Intent i =   new Intent(getBaseContext(), NovoSomActivity.class);
                i.putExtra("nome",n.nome);
                startActivity(i);
            }
        };
    }


    private List<Audio> getAudios() {

        List<Audio> notas = new ArrayList<Audio>();
        Log.i("log", getFilesDir().toString());
        File fileCerto = getExternalFilesDir(Environment.DIRECTORY_MUSIC);

        String[] lista = fileCerto.list();
        for (String s: lista){
            Log.i("log", s);
            notas.add(new Audio(s, R.drawable.nota));
        }
        return notas;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_som, menu);
        return true;
    }

}
