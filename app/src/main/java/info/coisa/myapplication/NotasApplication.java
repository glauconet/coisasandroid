package info.coisa.myapplication;

import android.app.Application;

/**
 * Created by glauco on 17/02/16.
 */
public class NotasApplication extends Application {

    private static NotasApplication instance = null;

    public static NotasApplication getInstance (){
        return instance;
    }
    @Override
    public void onCreate(){
        super.onCreate();

        instance = this;

    }


    @Override
    public void onTerminate(){
       super.onTerminate();


    }

}
